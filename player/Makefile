# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====
# *****************************************************************************
#  Description : Makefile to build iPreso's plugins
#  Auteur      : Marc Simonetti
# *****************************************************************************

PLUGIN_CMD=ledscreens.cmd
PLUGIN_INIT=ledscreens-init.sh
PLAYER_UPGRADE=ledscreens-upgrade.sh
PLUGIN_BIN=iLedBin

CURRENTDIR=$(shell basename `pwd`)

RM=@rm -f
CP=@cp
CC=@gcc

CC_OPT=-L.
INCLUDEDIR=-I/usr/include/iplayer
LIBDIR=-L/usr/lib/iplayer
LIBS=-liPlayer -ldl $(LIBDIR)
CFLAGS=-Wall -Wunused -g -O $(INCLUDEDIR)

# *****************************************************************************
# Operations
%.o: %.c
	@echo " [GCC] $@"
	$(CC) -o $@ -c $< $(CFLAGS)

# *****************************************************************************
# Dependances
iLedBin.o:        iLedBin.c

# *****************************************************************************
# Targets
all: $(PLUGIN_CMD) $(PLUGIN_BIN)
	@echo > /dev/null

clean:
	@echo -n "* Cleaning $(CURRENTDIR)... "
	$(RM) *.o $(PLUGIN_BIN)
	@echo "Ok"

install:
	@echo -n "* Installing $(CURRENTDIR)... "
	@mkdir -p $(DESTDIR)/var/lib/iplayer/cmd
	@cp $(PLUGIN_BIN) $(DESTDIR)/var/lib/iplayer/cmd/
	@cp $(PLUGIN_CMD) $(DESTDIR)/var/lib/iplayer/cmd/
	@cp $(PLUGIN_INIT) $(DESTDIR)/var/lib/iplayer/cmd/
	@mkdir -p $(DESTDIR)/etc/ipreso/upgrade.d
	@cp $(PLAYER_UPGRADE) $(DESTDIR)/etc/ipreso/upgrade.d/ledscreens.sh
	@echo "Ok"

# *****************************************************************************
# Edition de liens
$(PLUGIN_BIN): iLedBin.o
	@echo " [LNK] $@"
	$(CC) -o $@ $^ 
