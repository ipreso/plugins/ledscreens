#!/bin/bash

PATH=/sbin:/bin:/usr/sbin:/usr/bin:/var/lib/iplayer/cmd/
NAME=iplayer-ledscreens
BIN=/var/lib/iplayer/cmd/ledscreens.cmd
LEDBIN=/var/lib/iplayer/cmd/iLedBin
IWM_BACKUP=/usr/bin/iWM.guenine
IWM_REAL=/usr/bin/iWM

export DISPLAY=:0.0

test -x $BIN || exit 0

function start
{
    # We have to check this binary at every startup,
    # because it can be updated on iPlayer update
    echo "Configuring iWM symlink"
    if [ ! -L "$IWM_REAL" ] ; then
        mv "$IWM_REAL" "$IWM_BACKUP"
    fi
    rm -f "$IWM_REAL"
    ln -s "$BIN" "$IWM_REAL"

    echo "Starting iPlugin LED Screens"

    # Switch off the screens
    su - player -c "DISPLAY=:0.0 xrandr | grep \" connected\" | awk '{print \"xrandr --output \"\$1\" --off\"}' | sh"


    # Init screen
    su - player -c "DISPLAY=:0.0 ${BIN} -i"

    # Configure Box output to fit new LED Screen available reception
    su - player -c "DISPLAY=:0.0 xrandr --auto"
}

case "$1" in
  start)
        start
        ;;
  stop)
        exit 0
        ;;
  force-stop)
        stop
        sleep 1
        start
        ;;
  force-reload)
        exit 0
        ;;
  restart)
        stop
        sleep 1
        start
        ;;
  *)
    N=/etc/init.d/$NAME
    echo "Usage: $N {start|stop|restart|force-reload|force-stop}" >&2
    exit 1
    ;;
esac

exit 0
