#!/bin/bash
#
# Script executed on iPlayer's upgrade.
#
# Goal is to check and set correct binary for /usr/bin/iWM, even
# if overwritten by iPlayer's upgrade.

echo "Restart iplayer-ledscreens plugin."

systemctl stop iplayer-ledscreens || true
systemctl start iplayer-ledscreens

exit $?
